#!/usr/bin/env bash

# geometry has the format W H X Y
panel_height=25
x=1920
y=1055
panel_width=1920

font="Hack Nerd Font Mono:bold:size=14"
font2="Hack Nerd Font Mono:bold:size=18"

limit=16
{

echo -n "%{c}"
echo -n $(wlrctl toplevel list | awk -v LIMIT=$limit '{ printf("%s | ", $0 )}')


} 
#| zelbar -btm -fn "$font","$font2" -B "0x650000" -F "0xffffff" 
