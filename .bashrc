#!/bin/bash
# .bashrc

prompt_timestamp() {
	date +%s%N
	}

timer_start() {
	timer=${timer:-$(prompt_timestamp)}
	}

prompt_generator() {
	local timestring pathstring dirchar
	local delta_us=$(( ($(prompt_timestamp) - timer) / 1000 ))
	local ms=$(((delta_us / 1000) % 1000))
	local s=$(((delta_us / 1000000) % 60))
	timestring="$(( s >= 1 ? s : ms ))"
	timestring="$(printf '%03d' $timestring)"

	OFS="$IFS"
	IFS='/'
	[[ $PWD =~ ^$HOME ]] && pathstring='~'
	for d in ${PWD#$HOME}; do 
		[[ -z $d ]] && continue
		dirchar="${d:0:1}"
		[[ $dirchar = . ]] && dirchar="${d:1:1}"
		pathstring+="/$dirchar"
		done
	IFS="$OFS"
	
	
	PS1="$timestring $pathstring $ "
	unset timer
	}

PROMPT_COMMAND=prompt_generator
trap 'timer_start' DEBUG
	
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='eza -l -F --no-time --no-user --no-permissions --git --icons --group-directories-first --hyperlink'
alias ll='eza -l -F -T -L2 --no-time --git --icons --group-directories-first --hyperlink'
alias micro='micro -ruler=off -colorscheme torsten'
alias kernelsearch='xbps-query --regex -Rs "^linux[0-9.]+-[0-9._]+"'
alias kernellist='vkpurge list'
alias kernelwipe='sudo vkpurge rm $(vkpurge list | sort -Vr | tail -n +2)'
alias xterm='xterm -fn ""-xos4-*-medium-r-normal-*-14-*-*-*-*-*-*-*""'
alias cat='bat'
alias xdeb='xdeb -Sde'
alias neofetch="neofetch --uptime_shorthand 'tiny' --cpu_temp 'C'"
alias fastfetch="fastfetch --structure $(fastfetch --print-structure | sed -e 's/LocalIp://' -e 's/Swap//')"
alias me='echo "${USER}@$(hostname)"'
alias cmatrix='cmatrix -abk'
alias moc='mocp --theme darkdot_theme'
function free(){
	if [ "$1" == "-n" ]; then
			noti=$(/bin/free -hm | awk '/Speicher/ { print $3 " / " $2}')
			notify-send "$noti"
		else
			/bin/free -hm
		fi
	}
	
function o(){
	if [ -z "$1" ]; then
			xdg-open "$(find . | rofi -dmenu -i -theme torsten-o.rasi)"
		else
			xdg-open "$1"
		fi
}

function age(){
	echo $(\ls -alct --time-style=full-iso / | tail -1 | awk '{print $6}')
	}

function cl() { 
	clear && panes && ufetch 
	}

function jd(){
	j=$(date +%Y)
	k=${j:3:1}$(date +%j)
	echo $k
	}

[[ -f /etc/doas.conf ]] && alias sudo='doas'

eval "$(fzf --bash)"

# Pywal -n for without background  -q for quiet
wal -n -q -R

function myclock() { 
	# sourcing wal colors
	. ${HOME}/.cache/wal/colors.sh 
	oclock -hour "$color5" -minute "$color6" -jewel "$color7" -bd "$color2" -transparent & disown
	}


##git bare repo alias
#git init --bare
alias bgit='/bin/git --git-dir=$HOME/kiste/.void-setup/dotfiles.git --work-tree=$HOME'
alias wgit='/bin/git --git-dir=$HOME/kiste/.void-setup/wallpapers.git --work-tree=$HOME/kiste/pics/wallpaper'

#[[ -n $(lspci | grep -i virtualbox) ]] && alias bgit='/bin/git --git-dir=$HOME/kiste/.dotfiles/dotfiles.git --work-tree=$HOME' || alias bgit='/bin/git --git-dir=$HOME/test/dotfiles.git --work-tree=$HOME'
#bgit config --local status.showUntrackedFiles no

##herbstluftwm bash completition
[[ -f $HOME/git/herbstluftwm/share/herbstclient-completion.bash ]] && source $HOME/git/herbstluftwm/share/herbstclient-completion.bash

#setxkbmap de

#PS1="\[$(tput sgr0)\]\[\033[38;5;12m\]\w\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;14m\]\\$\[$(tput sgr0)\] \[$(tput sgr0)\]"

. "$HOME/.cargo/env"
. /usr/share/bash-completion/completions/herbstclient
export PATH=$PATH:$HOME/.local/bin:$HOME/kiste/bin

#colors to manpages
export MANPAGER="less -R --use-color -Dd+r -Du+b"

panes
cat $HOME/.cache/wttr.info
