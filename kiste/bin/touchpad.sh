#!/usr/bin/env bash

#dt=$(inxi -S | grep System | awk '{print $10}') #Bestimmung des Desktops
#case "$dt" in
#	"Xfce")
		id=$(xinput | grep -oi "Touchpad.*id=\S\+\s" | grep -o "id=[0-9]\+\S") && id=${id:3}
		#id des Touchpad ermitteln und formatierung des rückgabewertes (abschneiden von "id=")
		echo "id: " $id
		status=$(xinput list-props $id | grep "Device Enabled" | grep -o "[0-1]$")
		#ermitteln des aktuellen status des Touchpad
		echo "status: " $status
		case "$status" in
			"0") switch="1";;
			"1") switch="0";;
			esac

		echo "switch: " $switch
		xinput set-prop $id "Device Enabled" $switch
#		;;
#	"")
		#hier kommt dann das cinnamon zeugs hin mit gsettings
#		;;
#	esac

exit 0
