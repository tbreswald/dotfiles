#!/usr/bin/env bash


myvm=$(virsh -c qemu:///system list --all | tail -n +3 | awk '{print $2}' | rofi -dmenu -p "vm: " -theme $HOME/.config/rofi/themes/torsten.rasi -theme-str 'window{height: 320; width: 300;}')

virsh -c qemu:///system start $myvm && virt-viewer -f -a -c qemu:///system $myvm
