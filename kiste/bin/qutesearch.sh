#!/usr/bin/env bash

##rofi theme
[ -f "$HOME/.config/rofi/themes/torsten-1l.rasi" ] \
  && mytheme="$HOME/.config/rofi/themes/torsten-1l.rasi" \
  || mytheme="/usr/share/rofi/themes/DarkBlue.rasi"

searchterm=$(rofi -dmenu -p "search " -line 1 -theme $mytheme)
#echo "$searchterm"

[ ! -z "$searchterm" ] && qutebrowser "https://www.startpage.com/sp/search?query=${searchterm}"

# https://search.brave.com/search?q=${searchterm}
# https://www.startpage.com/sp/search?query=%s
