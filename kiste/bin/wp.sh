#!/usr/bin/env bash
domain="https://wallpapers.com"
topics=(laptop nature aesthetic background hacker space dark fall beach)
agent="whoever"

curdir=$(pwd)
dir=$HOME/kiste/pics/wallpaper/wallpapers.com
cd "$dir" || exit

for t in "${topics[@]}"
do
  images=($(wget -qO- --user-agent="$agent" "${domain}/${t}"| grep '<img'| grep 'src'| cut -d\" -f8 | cut -d/ -f4))

  for img in "${images[@]}"
  do
    #echo ${domain}/images/file/$img
	wget -c --user-agent="$agent" "${domain}/images/file/$img"
  done
done
cd $curdir
