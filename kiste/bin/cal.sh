#!/usr/bin/env bash
alacritty --class cal -o "font.size=8" --config-file $HOME/.config/alacritty/alacritty-cal.toml -e bash -c "cal -w3; read -t 25 -n 1 -s -r -p ''" & disown
