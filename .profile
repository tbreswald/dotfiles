export EDITOR=micro;
QT_STYLE_OVERRIDE=kvantum

export QT_QPA_PLATFORMTHEME="qt5ct"
#/usr/libexec/xfce-polkit &

wal -n -q -R
. "$HOME/.cargo/env"
curl -s 'wttr.in/{Hohenwestedt,Waren}?format=3' > $HOME/.cache/wttr.info
