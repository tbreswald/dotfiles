Console.WriteLine("Hallo Welt!");
Console.WriteLine("Mein Name ist Torsten");
string meinName = "Torsten";
Console.WriteLine(meinName);

int zahl = 20;
float zahl1 = 10.3f;
double zahl2 = 20.343;
decimal zahl3 = 58.23452m;
bool is_adult = false;
char character = 'P';

## math

int ergebnis = 15 + 2;
Console.WriteLine(ergebnis);

ergebnis = ergebnis + 10;
ergebnis += 10;
ergebnis++; 
##plus1

##methoden

void CountToThree()
{
	Console.WriteLine(1);
	Console.WriteLine(2);
	Console.WriteLine(3);
	Console.WriteLine("----");
}
CountToThree();

double Square(double number)
{
	double result = number * number;
	return result;
}

double quadrat = Square(5);
Console.WriteLine(quadrat);

void HelloPerson(string name, int age)
{
	Console.WriteLine("Hallo " + name +"(" + age + ")");
}

HelloPerson("Alina",35);

##interaktion
string eingabe = Console.Readline();
Console.WriteLine("---------");
Console.WriteLine(eingabe);

double number1 = Convert.ToDouble(Console.Readline());
double number2 = Convert.ToDouble(Console.Readline());
double result = number1 + number2;

Console.WriteLine(result);

##if

int alter = 13;

if (alter >= 18)
{
	Console.WriteLine("Du bist volljährig!");
}
else if (alter >= 16)
{
	Console.WriteLine("Du bist über 16!");
}
else
{
	Console.WriteLine("Du bist minderjährig!");
}

int alter = 19;
bool ausweisdabei = false;
bool elterdabei = false;

if ((alter >= 18 && ausweisdabei == true) || elterndabei == true)
{
	Console.WriteLine("Du darfst in den Film ab 18!");
}
else
{
	Console.WriteLine("Du darfst hier net rein!");
}


##switch

int tag = 3;

switch (tag)
{
	case 1:
		Console.WriteLine("Montag");
		break;
	case 2:
		Console.WriteLine("Dienstag");
		break;
	case 3:
		Console.WriteLine("Mittwoch");
		break;
	default:
		Console.WriteLine("ungültige Eingabe");
		break;
}

##arrays

string[] names = new string[3];
names[0] = "Alina";
names[1] = "Janek";
names[2] = "Friedrich";

Console.WriteLine(names[2]);

##2d-arrays

string[,] products = new string[2, 2];
products[0, 0] = "Nahrungsmittel";
products[0, 1] = "Hamburger";

##schleifen

int zahl = 0;

while(zhl <= 10)
{
	Console.WriteLine(zahl);
	zahl++;
}

for (int i = 0; i < 10; i++)
{
	Console.WriteLine(i);
}

string[] names = new string[]
{
	"Alina",
	"Hannes",
	"Peter",
	"Claus"
};

foreach (string name in names)
{
	Console.WriteLine(name);
}
